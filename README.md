# Rick and Morty

## STACK

[VueJS](https://vuejs.org/), Typescript, [Pinia](https://pinia.vuejs.org/), Vue-Router, Axios, [Bootstrap v5](https://getbootstrap.com/docs/5.2/getting-started/introduction/), SCSS, Prettier, [...]

## API

> https://rickandmortyapi.com/documentation

No need token or authentification

## Note

Making an interface based on the Rick And Morty API

#### 1 . List of cards

* **route**: /characters

	* The search bar must allow to filter on the name of the map at the view level.
	* Set up a loader
	* Set up a pagination

#### 2 . Detail of card

* **route**: /characters/:id

	* Display the info on the character
	* Manage for a smooth navigation
	* Set up a loader

## Setup

```sh
yarn install
```

### Compile and Hot-Reload for Development

```sh
yarn dev
```

> http://localhost:5173/

### Type-Check, Compile and Minify for Production

```sh
yarn build
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn lint
```

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).
