import axios from "axios";
import { defineStore } from "pinia";
import type { Character, CharacterLoading, Pagination, StatusType } from "@/interfaces";
import swal from "sweetalert";
import { apiUrl } from "@/main";

export const useCharactersStore = defineStore("characters", {
	state: () => ({
		noData: false,
		currentCharacter: {} as Character,
		characters: [] as Character[],
		loading: {
			characters: true,
			characterDetail: true,
		} as CharacterLoading,
		pagination: {} as Pagination,
		query: "",
	}),
	actions: {
		setLoading(data: { loadingKey: string; value: boolean }) {
			const { loadingKey, value } = data;

			if (!value) {
				setTimeout(() => {
					this.loading[loadingKey] = false;
				}, 1500);
			} else {
				this.loading[loadingKey] = value;
			}
		},

		async getCharacters() {
			const page = this.pagination.currentPage;
			const url = `${apiUrl}/character/?page=${page}`;

			await axios.get(url).then((response) => {
				const data = response.data;

				this.setCharacters(data);
			});
		},

		setCharacters(data: { info: Pagination; results: Character[] }) {
			this.pagination = data.info;
			this.characters = data.results;

			this.setLoading({
				loadingKey: "characters",
				value: false,
			});
		},

		async filterCharacterByName(name: string, status: StatusType = "alive") {
			const url = `${apiUrl}/character/?name=${name}&status=${status}`;

			axios
				.get(url)
				.then(({ data }) => {
					this.setCharacters(data);
				})
				.catch(() => {
					this.setLoading({
						loadingKey: "characters",
						value: false,
					});

					return swal({
						title: "Oops!",
						text: "Aucun résultat",
						icon: "error",
					});
				});
		},
	},
	getters: {
		// characters: (state) => state.characters,
	},
});
