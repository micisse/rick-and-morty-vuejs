import Home from "@/components/Home/Home.vue";
import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: "/",
			name: "Home",
			component: Home,
		},
		{
			path: "/characters",
			name: "Characters",
			component: () => import("../components/Characters/Characters.vue"),
		},
		{
			path: "/characters/:id",
			name: "CharacterDetail",
			component: () => import("../components/CharacterDetail/CharacterDetail.vue"),
		},
	],
});

export default router;
