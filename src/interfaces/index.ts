export interface Character {
	id: number;
	name: string;
	status: string;
	species: string;
	type: string;
	gender: string;
	image: string;
	location: {
		name: string;
		url: string;
	};
	created: string;
}

export interface CharacterLoading {
	[key: string]: boolean;
}

export interface Pagination {
	count: number;
	next: string;
	pages: number;
	prev: string;
	currentPage: number | string;
}

export type StatusType = "alive" | "dead" | "unknow";
