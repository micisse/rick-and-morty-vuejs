import Pagination from "@/components/Pagination/Pagination.vue";
import Loader from "@/components/Loader/Loader.vue";
import { defineComponent, reactive } from "vue";
import { useCharactersStore } from "@/stores/characters";
import { vue3Debounce } from "vue-debounce";
import type { StatusType } from "@/interfaces";

export default defineComponent({
	name: "Header",
	components: {
		Pagination,
		Loader,
	},
	directives: {
		debounce: vue3Debounce({ lock: true }),
	},
	props: {
		onDetailPage: Boolean,
	},
	setup() {
		const charactersStore = useCharactersStore();
		const reactiveState = reactive({ currentStatus: "alive" });
		const setStatus = (status: string) => {
			reactiveState.currentStatus = status as StatusType;
		};

		return { charactersStore, setStatus, reactiveState };
	},
	data() {
		return {
			query: "",
			status: ["alive", "dead", "unknown"],
		};
	},
	computed: {
		characters() {
			return this.charactersStore.characters;
		},

		loading() {
			return this.charactersStore.loading.characters;
		},
	},
	methods: {
		filterCharacterByName(value: string) {
			this.query = value;
			this.charactersStore.query = value;

			this.charactersStore.setLoading({
				loadingKey: "characters",
				value: true,
			});
			this.charactersStore.filterCharacterByName(this.query, this.reactiveState.currentStatus as StatusType);
		},
	},
});
